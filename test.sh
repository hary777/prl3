#!/bin/bash

#1 parametr a to retezec znaku
if [ $# -lt 1 ];
then 
    retezec="ABCDEFG";
else
    retezec=$1;
fi;

#pocet hran ve stromu * 2
pocet_procesoru=$(( (${#retezec} - 1) * 2))

#preklad cpp zdrojaku
mpic++ --prefix /usr/local/share/OpenMPI -Wall -o vuv vuv.cpp

#spusteni
mpirun --prefix /usr/local/share/OpenMPI -np $pocet_procesoru vuv $retezec

#uklid
rm -f vuv

