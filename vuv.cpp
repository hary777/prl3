#include <iostream>
#include <vector>
#include <string>
#include <cmath>

#include <mpi.h>

//#define DEBUG
//#define MEASURE

using namespace std;

//dopredne hrany jsou liche, zpetne jsou sude
#define IS_FORWARD_EDGE(e) ( ((e % 2) == 1) ? true : false )
#define IS_EXIST(e, count) ( (e <= count) ? true : false )

#define CHILD_LEFT(i)  ((2*i)+1)
#define CHILD_RIGHT(i) ((2*i)+2)
#define PARENT(i)      ((i-1)/2)

#define IS_LEFT_CHILD(parent, child) ( (CHILD_LEFT(parent) == child) ? true : false )

#define TAG_WS 0
#define TAG_P 1

#define WEIGHT    0
#define SUCCESSOR 1

#define RANK_CORRECT(i) (i-3) //hrany 0, 1, 2 se nepocitaji

#define IS_FIRST_EDGE(e) ((e == 3) ? true : false )
#define IS_LAST_EDGE(e)  ((e == 6) ? true : false )



/*vraci dalsi hranu k aktualni hrane,
hrana 0 neexistuje,
hrany 1 a 2 algoritmus nepocita, musi byt doplneny staticky,
tzn. naslednik hrany 1 je hrana 3, naslednik hrany 2 je hrana 1,
tudiz musi byt edge_id >= 3,
k tomu samozrejme celkovy pocet hran vcetne hrany 1 a 2*/
int euler_tour(int edge_id, int edge_count)
{
  if(IS_FORWARD_EDGE(edge_id))
  {
    int next_edge = (edge_id * 2) + 1;
    if(IS_EXIST(next_edge, edge_count))
    {
      return next_edge;
    }
    else
    {
      return edge_id + 1; //vzdycky existuje, ukazuje na zpetnou hranu
    }
  }
  else //BACKWARD_EDGE
  {
    int node_index = (edge_id / 2) - 1;
    int parent_node_index = (node_index - 1) / 2;
    if(IS_LEFT_CHILD(parent_node_index, node_index))
    {
      int next_edge = edge_id + 1;
      if(IS_EXIST(next_edge, edge_count))
      {
        return next_edge;
      }
      else
      {
        return next_edge / 2;
      }
    }
    else
    {
      return (edge_id / 2) - 1; //vzdycky existuje, ukazuje na zpetnou hranu
    }
  }
}

int edge_weight(int edge_id)
{
  if(IS_FORWARD_EDGE(edge_id))
  {
    return 1;
  }
  else
  {
    //osetreni posledniho prvku, zpetna hrana
    if(IS_LAST_EDGE(edge_id))
    {
      return 0; 
    }
    return -1;
  }
}

int main(int argc, char **argv)
{
  int numprocs; //pocet procesoru
  int myid;     //muj rank, od nuly
  MPI_Status mpi_mystat;      //struct- obsahuje kod- source, tag, error
  MPI_Request mpi_myreq, mpi_myreq2;

  //MPI INIT
  MPI_Init(&argc, &argv);                   // inicializace MPI
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs); // zjistíme, kolik procesů běží
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);     // zjistíme id svého procesu

  #ifdef MEASURE
  double t_start, t_stop;
  if(myid == 0){
    t_start = MPI_Wtime();
  }
  #endif


  int edge_id = myid + 3;   //cislo hrany, 0, 1, 2 se nepocita
  int edge_count = numprocs + 2;  //pocet hran, 0 neexistuje, 1 a 2 alg. nepocita 
  int next_edge = euler_tour(edge_id, edge_count);


  #ifdef DEBUG
  if(myid == 0)
  { 
    cerr << "edge count: " << edge_count << endl;
  }
  for(int i=0; i < numprocs; i++)
  {
    if (myid == i) {
      cerr << "next_edge:" << edge_id << "->" << next_edge << endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
  #endif


  /*korekce eulerovy cesty, hrany 1 a 2 nejsou potreba,
   tudiz hrana 6 je posledni v eulerove ceste, takze ukazuje sama na sebe
  */
  if(IS_LAST_EDGE(edge_id))
    next_edge = 6;


  //prideleni vahy
  int weight = edge_weight(edge_id);


  /*---suffix-sums--------------------------------*/
  int data_buf_send[2];
  int data_buf_recv[2];
  int successor = next_edge;
  int predecessor = -1; //hodnota neznameho predchudce
  int predecessor_recv;
  bool update = false;

  //prvni hrana nema predchudce, nastavime sebe samu
  if(IS_FIRST_EDGE(edge_id))
    predecessor = 3;

  for(int i=0; i < ceil(log2(edge_count)); i++)
  {
    if( !IS_LAST_EDGE(edge_id) && !IS_LAST_EDGE(successor) )
    {
      //poslu naslednikovi sveho predchudce
      MPI_Isend(&predecessor, 1, MPI_INT, RANK_CORRECT(successor), TAG_P, MPI_COMM_WORLD, &mpi_myreq);

      //prijmu od naslednika vahu a jeho naslednika
      MPI_Recv(data_buf_recv, 2, MPI_INT, RANK_CORRECT(successor), TAG_WS, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    
      MPI_Wait(&mpi_myreq, MPI_STATUS_IGNORE);
      update = true;
    }

    if( !IS_FIRST_EDGE(edge_id) && !IS_LAST_EDGE(edge_id) && !IS_FIRST_EDGE(predecessor) )
    {
      //prijmu od predchudce a zjistim jeho cislo
      MPI_Recv(&predecessor_recv, 1, MPI_INT, MPI_ANY_SOURCE, TAG_P, MPI_COMM_WORLD, &mpi_mystat);
    
      //poslu vahu a sveho naslednika
      data_buf_send[WEIGHT] = weight;
      data_buf_send[SUCCESSOR] = successor;
      MPI_Isend(data_buf_send, 2, MPI_INT, mpi_mystat.MPI_SOURCE, TAG_WS, MPI_COMM_WORLD, &mpi_myreq2);
    
      MPI_Wait(&mpi_myreq2, MPI_STATUS_IGNORE);
    
      //vypocet, aktualizace hodnot
      predecessor = predecessor_recv; //prepisu predchudce
    }

    if(update)
    {
      //vypocet, aktualizace hodnot
      weight += data_buf_recv[WEIGHT]; //pripocitam vahu
      successor = data_buf_recv[SUCCESSOR]; //prepisu naslednika
      update = false;
    }
  
    #ifdef DEBUG
    for(int i=0; i < numprocs; i++)
    {
      if (myid == i) {
        cerr <<"edge_id:" <<edge_id<< " succ:" << successor << " pred:" << predecessor 
        << " mpi_source:" << mpi_mystat.MPI_SOURCE+3 
        << " weight:" << abs(weight-2) << endl;      
      }
      MPI_Barrier(MPI_COMM_WORLD);
    }
    if(myid == 0){ cerr << "konec cyklu" << endl; }
    #endif  
  }
  /*-konec--suffix-sums--------------------------------*/

  #ifdef MEASURE
  if(myid == 0){
    t_stop = MPI_Wtime();
    cerr << t_stop - t_start << endl;
  }
  #endif


  #ifndef MEASURE
  if(IS_FORWARD_EDGE(edge_id))
  {
    //korekce hodnoty vahy
    weight = abs(weight - 2);

    //odeslani vahy do cpu 0
    MPI_Send(&weight, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
  }


  //prijem hodnot a vypis
  if(myid == 0)
  {
    //koren
    cout << argv[1][0] << ":0";

    //prvni hodnotu ma cpu 0
    cout << "," << argv[1][1] << ":" << weight;

    int j=2;
    for(int i=2; i < numprocs; i+=2)
    {
      MPI_Recv(&weight, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      cout << "," << argv[1][j] << ":" << weight;
      j++; 
    }
  }
  #endif


  MPI_Finalize();
  return 0;
}
